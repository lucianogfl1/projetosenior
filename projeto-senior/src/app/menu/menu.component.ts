import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { ItemService } from '../item/service/item.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: []
})
export class MenuComponent implements OnInit {
    items: MenuItem[];
    activeItem: MenuItem;
    emitir: boolean= false;

  constructor(private itemService: ItemService) {
    this.items = [
      // { label: 'Home', icon: 'pi pi-fw pi-home' },    
      { label: 'Cadastrar', icon: 'pi pi-fw pi-file', routerLink:'cadastrar' },
      { label: 'Listar', icon: 'pi pi-fw pi-pencil',routerLink:'listar' },   
    ];
    this.activeItem = this.items[0];
    this.itemService.emitirListagemDeItens.subscribe(
      (emitir) => {
        this.emitir = emitir;
        if(this.emitir == true){
          this.activeItem = this.items[1];          
        }else{
          this.activeItem = this.items[0];          
        }           
      }      
    );   
  }

  ngOnInit(): void { 
  }
  alternaMenu(){    
    if(this.activeItem == this.items[1]){
      this.activeItem = this.items[0]
    }
    else{
      this.activeItem = this.items[1]
    }
  }
}
