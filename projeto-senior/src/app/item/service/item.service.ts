import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  emitirListagemDeItens = new EventEmitter<boolean>();
  objetoVazio: any[] = [{
    dataFabricacao: "",
    dataValidade: "",
    nome: "",
    perecivel: "",
    preco: "",
    quantidade: "",
    unidade: ""
  }];
  elementoVazio: any = {
    dataFabricacao: "",
    dataValidade: "",
    nome: "",
    perecivel: "",
    preco: "",
    quantidade: "",
    unidade: ""
  };
  listaDeItens: any[] = [];
  constructor() { }
  //retorna um item
  getItem() {
    return '';

  }
  getItens(): any {
    this.listaDeItens = [];
    if (localStorage.cont) {
      for (let i = 1; i <= localStorage.cont; i++) {
        const currentElement = String(localStorage.getItem(i + '') || '');
        let elementoJson = JSON.parse(currentElement);
        if ((elementoJson.nome !== this.elementoVazio.nome) && (elementoJson.unidade !== this.elementoVazio.unidade)) {
          this.listaDeItens.push(elementoJson);
        }
      }
      return this.listaDeItens;
    }
    else {
      return this.objetoVazio;
    }
  }
  getUnidades() {
    return ["Litro", "Quilograma", "Unidade"];
  }
  getStates() {
    return [{ label: 'Não', value: 'false' }, { label: 'Sim', value: 'true' }];
  }
  createItem(form: any): boolean {
    if (typeof (Storage) !== 'undefined') {
      if (localStorage.cont) {
        localStorage.cont = Number(localStorage.cont) + 1;
      } else {
        localStorage.cont = 1;
      }
      let element = JSON.stringify(form);
      localStorage.setItem(localStorage.cont, element);
      return true;
    }
    else {
      console.log("O navegador usado não tem suporte para usar o LocalStorage");
      return false;
    }
  }
  listarItens(emitir: boolean) {
    this.emitirListagemDeItens.emit(emitir);
  }
  apagarItem(item: any) {
    this.listaDeItens = [];
    for (let i = 1; i <= localStorage.cont; i++) {
      const currentElement = String(localStorage.getItem(i + '') || '');
      let elementoJson = JSON.parse(currentElement);
      if ((item.nome == elementoJson.nome) && (item.unidade == elementoJson.unidade)) {
        elementoJson = this.elementoVazio;
        let element = JSON.stringify(elementoJson);
        localStorage.setItem(i + '', element);
      } else {
        this.listaDeItens.push(elementoJson);
      }

    }
    return true;
  }

  alterarIten(itemNew: any, nome: string) {
    this.listaDeItens = [];
    for (let i = 1; i <= localStorage.cont; i++) {
      const currentElement = String(localStorage.getItem(i + '') || '');
      let elementoJson = JSON.parse(currentElement);
      
      if ((nome == elementoJson.nome)) {        
        elementoJson = itemNew;
        let element = JSON.stringify(elementoJson);
        localStorage.setItem(i + '', element);
      }

    }
    return true;
  }
}
