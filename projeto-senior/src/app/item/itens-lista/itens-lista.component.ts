import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemService } from '../service/item.service';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { Calendar } from 'primeng/calendar';
@Component({
  selector: 'app-itens-lista',
  templateUrl: './itens-lista.component.html',
  styleUrls: ['./itens-lista.component.css']
})
export class ItensListaComponent implements OnInit {
  itens: any[] = [];
  itemSelecionado: any = null;
  itemAntigo: any = null;
  itemApagado: boolean = false;
  itemDialog: boolean = false;
  listaDeUnidade: string[];
  stateOptions: any[];  
  dataValidade:any;
  dataFabricacao:any; 
  gravou: boolean=false;
  nomeItem:string='';
  constructor(private itemService: ItemService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.itens = itemService.getItens();
    this.listaDeUnidade =itemService.getUnidades();
    this.stateOptions = itemService.getStates();
  }

  ngOnInit(): void {
  }
  cadastrarItens() {
    this.itemService.listarItens(false);
  }
  deleteItem(item: any) {
    this.itemSelecionado = item;
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Deseja Apagar?', detail: 'Confirmar apagar o item ' + item.nome + '' });
  }
  onConfirm() {
    this.messageService.clear('c');
    this.itemApagado = false;
    if (this.itemSelecionado !== null) {
      this.itemApagado = this.itemService.apagarItem(this.itemSelecionado);
      if (this.itemApagado == true) {
        this.itens = this.itemService.getItens();
        this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Item Apagado', life: 2000 });
      } else {
        this.messageService.add({ key: 'bc', severity: 'error', summary: 'Erro', detail: 'Não foi possível apagar o Item', life: 2000 });
      }
    }
  }

  onReject() {
    this.messageService.clear('c');
    this.itemSelecionado = null;
  }
  editItem(item:any) { 
    this.dataValidade =null;
    this.dataFabricacao = null;
    this.itemSelecionado = item;
    this.itemAntigo=this.itemSelecionado;
    this.nomeItem = this.itemAntigo.nome;
    var parts = this.itemSelecionado.dataValidade.split("/");
    this.dataValidade = new Date(parts[2], parts[1] - 1, parts[0]);
    var parts2 = this.itemSelecionado.dataFabricacao.split("/");
    this.dataFabricacao = new Date(parts2[2], parts2[1] - 1, parts2[0]);
    this.itemSelecionado.dataValidade = this.dataValidade;
    this.itemSelecionado.dataFabricacao = this.dataFabricacao;
    this.itemAntigo=this.itemSelecionado;
    this.itemDialog = true;
  }
  onCancel(){
    this.itemDialog = false;
    this.itens = this.itemService.getItens();
    this.itemSelecionado= null;
  }
  onSalvar(){
    if(this.itemSelecionado.dataValidade){
      let dataFormatadaValidade = ((this.itemSelecionado.dataValidade.getDate() )) + "/" + ((this.itemSelecionado.dataValidade.getMonth() + 1)) + "/" + this.itemSelecionado.dataValidade.getFullYear();
      this.itemSelecionado.dataValidade = dataFormatadaValidade;
    } 
    if(this.itemSelecionado.dataFabricacao){
      let dataFormatadaFabricacao = ((this.itemSelecionado.dataFabricacao.getDate() )) + "/" + ((this.itemSelecionado.dataFabricacao.getMonth() + 1)) + "/" + this.itemSelecionado.dataFabricacao.getFullYear();
      this.itemSelecionado.dataFabricacao = dataFormatadaFabricacao;
    }    
    
    this.gravou = this.itemService.alterarIten(this.itemSelecionado,this.nomeItem);
    
    if(this.gravou){
      this.itemDialog = false;
      this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Item Alterado', life: 2000 });
      this.itemSelecionado= null;
      this.itemAntigo = null;
      this.itens = this.itemService.getItens();
    }
  }
  validaInputNome() {
    if(this.itemSelecionado.nome == '')
    {return true}else{return false};
  }

}
