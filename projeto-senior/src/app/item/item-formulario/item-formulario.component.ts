import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ItemService } from '../service/item.service';
import { PrimeNGConfig } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { typeWithParameters } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-item-formulario',
  templateUrl: './item-formulario.component.html',
  styleUrls: ['./item-formulario.component.css'],
  providers: [MessageService]
})
export class ItemFormularioComponent implements OnInit {
  itemSelecionado: string;
  listaDeUnidade: string[];
  stateOptions: any[];
  itemGravado: boolean = false;
  listar: boolean = true;
  erroNome: string = "";
  medidaUsada: string = "Lt";
  tamanhoString: string = "";
  limiteDeDigitos: boolean = true;
  isLitro: boolean = true;
  isQuilo: boolean = false;
  isUnidade: boolean = false;
  quantidadeMaxima: any = 999;
  obrigaValidade: boolean = false;
  produtoVencido: boolean = false;

  itemFormulario: FormGroup;

  constructor(private itemService: ItemService, private formBuilder: FormBuilder, private primengConfig: PrimeNGConfig, private messageService: MessageService) {
    this.itemSelecionado = itemService.getItem();
    this.listaDeUnidade = itemService.getUnidades();
    this.stateOptions = itemService.getStates();
    this.itemFormulario = this.formBuilder.group({
      nome: [null],
      unidade: [this.listaDeUnidade[0]],
      quantidade: [null],
      preco: [null],
      perecivel: ['false'],
      dataValidade: [null],
      dataFabricacao: [null]
    });
  }

  ngOnInit(): void { this.primengConfig.ripple = true; }
  onSubmit() {
    //gravar no localStorage
    this.itemGravado = false;
    this.validaFormulario();
  }
  validaFormulario() {  

    if ((this.itemFormulario.get('nome')?.value) && (this.itemFormulario.get('dataFabricacao')?.value) && (this.itemFormulario.get('preco')?.value) && (this.itemFormulario.get('perecivel')?.value)) {
      if(this.itemFormulario.get('dataValidade')?.value){
        let dataFormatadaValidade = ((this.itemFormulario.get('dataValidade')?.value.getDate() )) + "/" + ((this.itemFormulario.get('dataValidade')?.value.getMonth() + 1)) + "/" + this.itemFormulario.get('dataValidade')?.value.getFullYear();
        this.itemFormulario.value.dataValidade = dataFormatadaValidade;
      }     
      let dataFormatadaFabricacao = ((this.itemFormulario.get('dataFabricacao')?.value.getDate() )) + "/" + ((this.itemFormulario.get('dataFabricacao')?.value.getMonth() + 1)) + "/" + this.itemFormulario.get('dataFabricacao')?.value.getFullYear();
      this.itemFormulario.value.dataFabricacao = dataFormatadaFabricacao;
      this.itemGravado = this.itemService.createItem(this.itemFormulario.value);
      if (this.itemGravado) {
        this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Item Gravado', life: 2000 });
        this.itemFormulario.reset();
        this.limiteDeDigitos = true;
        this.isLitro = true;
        this.isQuilo = false;
        this.isUnidade = false;
        this.quantidadeMaxima = 999;
        this.obrigaValidade = false;
        this.produtoVencido = false;
      }
      else {
        this.messageService.add({ key: 'bc', severity: 'error', summary: 'Erro', detail: 'O navegador usado não tem suporte para usar o LocalStorage', life: 2000 });
      }
    }
    else {
      this.messageService.add({ key: 'bc', severity: 'warn', summary: 'Aviso', detail: 'Preencha os dados obrigatórios', life: 2000 });
    }
  }
  listarItens() {
    this.itemService.listarItens(true);
  }
  validaInput(atributo: string) {
    return !this.itemFormulario.get(atributo)?.valid && this.itemFormulario.get(atributo)?.touched;
  }
  limitDigits(event: any): boolean {
    if (!this.isUnidade) {
      if (this.itemFormulario.get('quantidade')?.value) {
        this.tamanhoString = (this.itemFormulario.get('quantidade')?.value).toString();
      }

      if (this.tamanhoString) {
        if (this.tamanhoString.length >= 3) {
          this.limiteDeDigitos = false;
          return false;
        } else {
          this.limiteDeDigitos = true;
          return true;
        }
      } else {
        this.limiteDeDigitos = true;
        return true;
      }
    }
    else {
      this.limiteDeDigitos = true;
      return true;
    }

  }
  validaQuantidade() {
    return this.limiteDeDigitos;
  }
  verificaUnidade() {
    if (this.itemFormulario.get('unidade')?.value == "Litro") {
      this.isLitro = true;
      this.isQuilo = false;
      this.isUnidade = false;
      this.medidaUsada = "Lt";
      this.quantidadeMaxima = 999;
    }
    if (this.itemFormulario.get('unidade')?.value == "Quilograma") {
      this.isLitro = false;
      this.isQuilo = true;
      this.isUnidade = false;
      this.medidaUsada = "Kg";
      this.quantidadeMaxima = 999;
    }
    if (this.itemFormulario.get('unidade')?.value == "Unidade") {
      this.isLitro = false;
      this.isQuilo = false;
      this.isUnidade = true;
      this.medidaUsada = "Un";
      this.quantidadeMaxima = false;
    }

  }
  atualizaPerecivel() {
    if (this.itemFormulario.get('perecivel')?.value == 'true') {
      this.obrigaValidade = true;
    } else {
      this.obrigaValidade = false;
    }

  }
  verificaVencimento() {
    var now = new Date;

    if (this.itemFormulario.get('dataValidade')?.value) {
      if (this.itemFormulario.get('dataValidade')?.value.getTime() < now.getTime()) {
        this.produtoVencido = true;
      } else {
        this.produtoVencido = false;
      }
    }
  }
  verficaDataFabrica() {
    if ((this.itemFormulario.get('dataValidade')?.value) && (this.itemFormulario.get('dataFabricacao')?.value)) {

      if (this.itemFormulario.get('dataValidade')?.value.getTime() < this.itemFormulario.get('dataFabricacao')?.value.getTime()) {
        return true;
      } else {
        return false;
      }


    } else {
      return false;
    }
  }

}
