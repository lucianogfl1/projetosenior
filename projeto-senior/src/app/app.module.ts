/*Module*/
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TabMenuModule} from 'primeng/tabmenu';
import {RippleModule} from 'primeng/ripple';
import {SelectButtonModule} from 'primeng/selectbutton';
import {CalendarModule} from 'primeng/calendar';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast'
import {ToolbarModule} from 'primeng/toolbar';
import {TableModule} from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {DialogModule} from 'primeng/dialog';
import {InputNumberModule} from 'primeng/inputnumber';
import {InputMaskModule} from 'primeng/inputmask';
import { CurrencyMaskModule } from "ng2-currency-mask";
/*Service*/
import {ItemService} from './item/service/item.service'
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
/*Component*/
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ItemFormularioComponent } from './item/item-formulario/item-formulario.component';
import { ItensListaComponent } from './item/itens-lista/itens-lista.component';
/*routing */
import { routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ItensListaComponent,
    ItemFormularioComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TabMenuModule,
    routing,
    ReactiveFormsModule,
    InputTextModule,
    DropdownModule,
    BrowserAnimationsModule,
    RippleModule,
    SelectButtonModule,
    CalendarModule,
    ButtonModule,
    ToastModule,
    ToolbarModule,
    TableModule,
    ConfirmDialogModule,
    DialogModule,
    InputNumberModule,
    InputMaskModule,
    CurrencyMaskModule
  ],
  providers: [ItemService,MessageService,ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
