import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import {ItemFormularioComponent} from './item/item-formulario/item-formulario.component'
import{ItensListaComponent} from './item/itens-lista/itens-lista.component'
const APP_ROUTES: Routes = [ 
        {path:'cadastrar', component: ItemFormularioComponent},
        {path:'listar', component: ItensListaComponent},
        {path:'', component: ItemFormularioComponent}
   
];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(APP_ROUTES);