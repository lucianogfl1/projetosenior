## install project dependencies
in paste projeto-senior run `npm install` for install the dependencies. 

## Development server

 Open the paste projeto-senior and run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
